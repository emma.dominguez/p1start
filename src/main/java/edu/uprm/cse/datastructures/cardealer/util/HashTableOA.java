package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class HashTableOA<K, V> implements Map<K, V> {

	////////////////////////MAP ENTRY////////////////////////
	public static class MapEntry<K,V> {
		private K key;
		private V value;

		public K getKey() {
			return key;
		}
		public void setKey(K key) {
			this.key = key;
		}
		public V getValue() {
			return value;
		}
		public void setValue(V value) {
			this.value = value;
		}
		public MapEntry(K key, V val) {
			super();
			this.key = key;
			this.value = val;

		}
	}

	/////////////OPEN ADRESSING BUCKETS//////////////////////
	private static class OABucket{
		private boolean inUse;
		private MapEntry ME;

		public boolean getinUsed (){
			return inUse;
		}
		public void setinUse() {
			inUse=!inUse;
		}
		public MapEntry getME() {
			return this.ME;
		}
		public void setME(MapEntry ME) {
			this.ME = ME;
		}
	}
	
	/////////////////////Hash Table with OA and MAP Implementation////////////////////////////////////////
	private int CurrentSize;
	private OABucket[] buckets;
	private static final int DEFAULT_SIZE=10;
	private Comparator<K> comp;

	/////////////////////CONSTRUCTOR///////////////////////////////
	public HashTableOA() {
		this(DEFAULT_SIZE);
	}
	public HashTableOA(int size) {
//		this.CurrentSize=0;
//		this.buckets= new OABucket[size];
//		for(int i=0; i<this.buckets.length; i++) {
//			this.buckets[i] = (OABucket) new SortedList<MapEntry<K,V>>();
//		}
	}
	/////////////////////////// HASH FUNCTIONS /////////////////
	private int PrimHashFucntion(K key) {
		return (int) (this.hashCode() % Math.sqrt(229) + 1);
	}

	private int SecHashFunction(K Key) {
		return (int) (7+ this.hashCode() % Math.sqrt(this.buckets.length));
	}

	//////////////////////////// MAP IMPLEMENTATION///////////////
	@Override
	public int size() {
		return this.CurrentSize;
	}

	@Override
	public boolean isEmpty() {
		return this.CurrentSize==0;
	}

	@Override
	public V get(K key) {
		for (Object buck: this.buckets) {
			SortedList<MapEntry<K,V>> B = (SortedList<MapEntry<K, V>>) buck;
			for (MapEntry k: B) {
				if (key.equals(k)) {
					return (V) k.getValue();
				}
			}
		}
		return null;
	}

	@Override
	public V put(K key, V value) {
		V valToReturn= this.get(key);
		int hashNum1=this.PrimHashFucntion(key);
		int hashNum2=this.SecHashFunction(key);
		
		if (!this.buckets[hashNum1].inUse) {
			SortedList<MapEntry<K,V>> list = (SortedList<MapEntry<K, V>>) this.buckets[hashNum1];
			list.add(new MapEntry(key,value));
			this.CurrentSize++;
		}
		
		else if (!this.buckets[hashNum2].inUse) {
			SortedList<MapEntry<K,V>> list = (SortedList<MapEntry<K, V>>) this.buckets[hashNum1];
			list.add(new MapEntry(key,value));
			this.CurrentSize++;
		}
		
		else {
			for (Object buck: this.buckets) {
				SortedList<MapEntry<K,V>> B = (SortedList<MapEntry<K, V>>) buck;
				for (MapEntry k: B) {
					if(B.isEmpty()) {
						B.add(new MapEntry(key, value));
						this.CurrentSize++;
						break;
					}
					
				}
			}
		}
		return valToReturn;
	}

	@Override
	public V remove(K key) {
		V val;
		for (Object buck: this.buckets) {
			SortedList<MapEntry<K,V>> B = (SortedList<MapEntry<K, V>>) buck;
			for (MapEntry k: B) {
				if (key.equals(k)) {
					val = (V) k.getKey();
					B.remove(k);
					this.CurrentSize--;
				return val;
				}
			}
		}
		return null;
	}

	@Override
	public boolean contains(K key) {
		return this.get(key)!=null;
	}

	@Override
	public SortedList<K> getKeys() {
		SortedList<K> result = new CircularSortedDoublyLinkedList<K>(this.comp);
		for (Object keyBucket: this.buckets) {
			SortedList<MapEntry<K,V>> keysList = (SortedList<MapEntry<K,V>>) keyBucket;
			for (MapEntry<K,V> key : keysList) {
				result.add((K) key);
			}
		}
		return result;
	}

	@Override
	public SortedList<V> getValues() {
		SortedList<V> result = new CircularSortedDoublyLinkedList<V>((Comparator<V>) this.comp);
		for (Object valueBucket: this.buckets) {
			SortedList<MapEntry<K,V>> valueList = (SortedList<MapEntry<K,V>>) valueBucket;
			for (MapEntry<K,V> value : valueList) {
				result.add((V) value);
			}
		}
		return result;
	}
}
