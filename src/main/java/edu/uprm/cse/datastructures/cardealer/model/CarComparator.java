package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

@SuppressWarnings("hiding")
public class CarComparator implements Comparator<Car> {

	@Override
	public int compare(Car c1, Car c2) {
		//easy access variables to compare brands
		String c1Brand = c1.getCarBrand();
		String c2Brand = c2.getCarBrand();
		int Brand = c1Brand.compareTo(c2Brand);
		//easy access variables to compare models
		String c1Model = c1.getCarModel();
		String c2Model = c2.getCarModel();
		int Model = c1Model.compareTo(c2Model);
		//eassy access variables to compare options
		String c1Option = c1.getCarModelOption();
		String c2Option = c2.getCarModelOption();
		int Option = c1Option.compareTo(c2Option);
		//comparator, if c1>c2 returns a positive number, if c1==c1 returns 0, c1<c2 returns a negative int.
		if (Brand>0) {return 1;}
		else if (Brand<0) { return -1;}
		else{
			if (Model>01) {return 1;}
			else if (Model<0) {return -1;}
			else {
				return Option;
			}
		}
	}

}
