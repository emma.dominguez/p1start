package edu.uprm.cse.datastructures.cardealer.util;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {

//--------------------------------------------------------------------NODE------------------------------------------------	
	private static class Node<E>{
		private E element;
		private Node<E> next;
		private Node<E> previous;

		public Node(E element, Node<E> next, Node<E> previous) {
			super();
			this.element=element;
			this.next=next;
			this.previous=previous;
		}
		public Node() {
			super();
		}
		public E getElement() {
			return element;
		}
		public Node<E> getNext(){
			return next;
		}
		public Node<E> getPrevious(){
			return previous;
		}
		public void setElement(E e) {
			this.element=e;
		}
		public void setNext(Node<E> newNode)
		{
			this.next=newNode;
		}
		public void setPrevious(Node<E> prevNode) {
			this.previous=prevNode;
		}

	}
//----------------------------------------------------------ITERATOR-----------------------------------------------------	
	private class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E>{
		private Node<E> next;
		private Node<E> previous;

		public CircularSortedDoublyLinkedListIterator() {
			this.next= (Node<E>) header.getNext();
			this.previous= (Node<E>) header.getPrevious();
		}

		@Override
		public boolean hasNext() {
			return next!=header;
		}
		public boolean hasPrevious() {
			return previous!=header;
		}

		@Override
		public E next() {
			if (this.hasNext()) {
				E item = this.next.getElement();
				this.next=this.next.next;
				return item;
			}
			else {
				throw new NoSuchElementException();
			}
		}

		public E previous() {
			if (this.hasPrevious()) {
				E item = this.previous.getElement();
				this.previous=this.previous.previous;
				return item;
			}
			else {
				throw new NoSuchElementException();
			}
		}


	}

//-------------------------------------------------CSDLL Constructor--------------------------------------------------------
	private int currentSize;
	private Node<E> header; 
	private Node<E> previous; 
	private Comparator<E> carComp;

	public CircularSortedDoublyLinkedList(Comparator<E> c) {
		this.currentSize=0;
		this.header = new Node<>(); 
		this.header.setPrevious(this.header);
		this.header.setNext(this.header);
		this.carComp=c;
	}


	@Override
	//method to call the iterator implemented above
	public Iterator<E> iterator() {
		return new CircularSortedDoublyLinkedListIterator();
	}

	@Override
	//compares objects before adding them so that the list stays sorted
	public boolean add(E obj) {
		//if the list is empty the first element will have a previous and a next pointing to header Node
		if (this.isEmpty()) {
			Node<E> firstTemp= new Node<E>(obj,header,header);
			header.setNext(firstTemp);
			header.setPrevious(firstTemp);
			currentSize++;
			return true;
		}	
		//creates two node variables for easy access
		Node<E> newNode = new Node<E>(obj, null, null);
		Node<E> headerNode = header;
		//goes through the list comparing the new obj and setting it's previous node and it's next
		for(headerNode=this.header.getNext(); headerNode!=header; headerNode=headerNode.getNext()) {
			if(this.carComp.compare(headerNode.getElement(), newNode.getElement())>0) {
				newNode.setNext(headerNode);
				newNode.setPrevious(headerNode.getPrevious());
				headerNode.getPrevious().setNext(newNode);
				headerNode.setPrevious(newNode);
				currentSize++;
				return true;
			}
		}
		//if it goes through the list and it doesn't find an obj smaller than the new obj, it adds it at the end of the list
		newNode.setPrevious(header.getPrevious());;
		newNode.setNext(header);
		header.setPrevious(newNode);
		newNode.getPrevious().setNext(newNode);
		currentSize++;
		return true;
	}

	@Override
	//returns the amount of obj in the list
	public int size() {
		return this.currentSize;
	}

	@Override
	//removes an obj by finding the obj
	public boolean remove(E obj) {
		//goes through the nodes in this until it find the object and then removes it
		//by changing it's previous to it's next node and it's next node's previous to it
		//s previous node and then setting everything in the obj node to null while decreasing current size.
		for (Node<E> temp=this.header.getNext(); temp!=this.header; temp= temp.getNext()) {
			if (temp.getElement().equals(obj)) {
				temp.getPrevious().setNext(temp.getNext());
				temp.getNext().setPrevious(temp.getPrevious());
				temp.setNext(null);
				temp.setPrevious(null);
				temp.setElement(null);
				this.currentSize--;
				return true;
			}
		}

		return false;
	}

	@Override
	//removes the obj at a certain index
	public boolean remove(int index) {
		//checks bounds
		if (index>this.size() || index<0) {
			throw new IndexOutOfBoundsException();
		}
		int counter=0;
		//easy access 
		Node<E> temp = this.header.getNext();
		//looking for the element with specified index and uses the obj remove
		while(temp!= null) {
			if (counter == index) {
				return this.remove(temp.getElement());
			}else {
				counter++;
				temp= temp.getNext();
			}

		}
		return false;
	}

	@Override
	//goes through all the list removing all instances of the same obj with a for each.
	public int removeAll(E obj) {
		int count=0;
		for (E e: this) {
			if (e.equals(obj)) {
				this.remove(e);
				count++;
			}
		}
		return count;
	}

	@Override
	//returns the first element after the dummy header, element at position 0
	public E first() {
		return this.header.getNext().getElement();
	}

	@Override
	//returns the element previous dummy header, element at current size
	public E last() {
		return this.header.getPrevious().getElement();
	}

	@Override
	//returns an element at the specified index
	public E get(int index) {
		//check bounds
		if (index<0 || index> this.size()) {
			throw new IndexOutOfBoundsException();
		}
		int count=0;
		//easy access
		Node<E>  temp = this.header.getNext();
		//searches for the element 
		while (count!=index){
			temp = temp.getNext();
			count++;
		}
		//returns the element at x index
		return temp.getElement();
	}

	@Override
	//loops through the list with a for each and removes every element, setting it back to clear
	public void clear() {
		for (E e: this) {
			this.remove(e);
		}
	}

	@Override
	//if the first index of obj is greater than 0 than, it is in the list
	public boolean contains(E e) {
		return this.firstIndex(e)>0;
	}
	
	@Override
	//if the list has no elements, than the size is 0
	public boolean isEmpty() {
		return this.size()==0;
	}

	@Override
	//returns the first time element e is found
	public int firstIndex(E e) {
		int counter=0;
		//goes through the list with a for each, if it finds it adds 1 to counter, 
		//else adds one. Returns counter or -1 if not found.
		for (E obj: this) {
			if (obj.equals(e)) {
				return counter++;
			}
			else {
				counter++;
			}
		}
		return -1;
	}
	
	@Override
	//returns the last time the element e is found
	public int lastIndex(E e) {
		int counter=0;
		//goes through the list starting at the end and finding the first time it appears;
		for (Node<E> temp= this.header.getPrevious(); temp!=header; temp= temp.getPrevious()) {
			if (temp.getElement().equals(e)) {
				//the index is the subtraction of the list size - counter -1 for that empty node in the list
				return (this.size()-counter-1);
			}
			else {
				counter++;
			}
		}
		//if not found return -1.
		return -1;
	}

}
