package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

@Path("/cars") //creates initial path
public class CarManager {
	//creates the list of cars used in the dealer
	private final CircularSortedDoublyLinkedList<Car> carsList = new CarList().getInstance();

	//gets all cars initially to show in the page and adds the cars to the array of cars
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		Car[] carList = new Car[carsList.size()];
		int i=0;
		for (Car c: carsList) {
			carList[i]=c;
			i++;
		}
		return carList;
	}
	//to add a new car to the cars array 
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car newCar){
		CircularSortedDoublyLinkedList post= new CircularSortedDoublyLinkedList<Car>(null);
		//if there's another car with the same id, return an error
		for (Car c : carsList) {
			if (c.getCarId()==newCar.getCarId()) {
				return Response.status(Response.Status.NOT_ACCEPTABLE).build();
			}
		}
		//else add car
		carsList.add(newCar);
		return Response.status(201).build();
	}    

	//returns the car with the given id
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id) {
		//looks for car c with the same id and returns it, else, not found exception
		for (Car c: carsList) {
			if (c.getCarId()== id) {
				return c;
			}
		}
		throw new NotFoundException();
	}

	//updates information of a car
	@PUT
	@Path("{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car newCar, @PathParam("id") long id) {
		//looks for the car with the given id and removes it, then adds a new car in the same id.
		for(Car c: carsList) {
			if(c.getCarId()==id) {
				carsList.remove(c);
				carsList.add(newCar);
				return Response.status(200).build();
			}
		}
		throw new NotFoundException();
	}
	
	//Deletes a car with the given id
	@DELETE
	@Path("{id}/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteCar(@PathParam("id") long id) {
		//goes through the list until it finds the car with the id and then removes it
		for(Car c : carsList) {
			if(c.getCarId()==id) {
				carsList.remove(c);
				return Response.status(200).build();
			}
		}


		throw new NotFoundException();
	}


}
