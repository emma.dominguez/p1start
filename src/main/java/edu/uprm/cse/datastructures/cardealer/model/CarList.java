package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

public class CarList {
	//help initiate a CSDLL of Cars for the API
	private static CircularSortedDoublyLinkedList<Car> carList= new CircularSortedDoublyLinkedList<Car>(new CarComparator());
	
	public static CircularSortedDoublyLinkedList<Car> getInstance() {
		return carList;
	}
	public static void resetCars() {
		carList.clear();
	}
	
}
